<?php

session_start();
require '../config/config-login.php';
//require "../header.php";
require '../config/dbconf.php';
require '../config/globalcon.php';
require '../includes/functions.php';

try {
    $param = json_decode(file_get_contents('php://input'));
    $jjnDb = new JjnDb();
    if (isset($param->next)) {
        getNextRow($param->next);
        return;
    }

    $result = $jjnDb->searchList("");
    if (!$result->response) {
        echo json_encode($result);
        return;
    }

    $list = $result->list;
    setRowList($list);

    $firstRow = $list[0];
    if (count($list) > 1) {
        getResult($firstRow, 1);
    } else {
        getResult($firstRow, 0);
    }
} catch (Exception $ex) {
    if (isset($jjnDb) && isset($jjnDb->conn)) {
        $jjnDb->conn->rollback();
    }

    $reply = new stdClass();
    $reply->response = "false";
    $reply->msg = $ex;
    echo json_encode($reply);
}

function setRowList($list) {
    $_SESSION['rowList'] = $list;
}

function getNextRow($nextParam) {
    if (!isset($_SESSION['nextRow'])) {
        $reply = new stdClass();
        $reply->response = "false";
        $reply->msg = "No next row available";
        echo json_encode($reply);
        return;
    }
    
    $nextRowInfo = $_SESSION['nextRow'];
    
    if ($nextParam->uid != $nextRowInfo->uid) {
        $reply = new stdClass();
        $reply->response = "false";
        $reply->msg = "Next row was expired";
        echo json_encode($reply);
        return;
    }
    
    if ($nextParam->nextIndex != $nextRowInfo->nextIndex) {
        $reply = new stdClass();
        $reply->response = "false";
        $reply->msg = "Next row index is invalid";
        echo json_encode($reply);
        return;
    }
    
    if (!isset($_SESSION['rowList'])) {
        $reply = new stdClass();
        $reply->response = "false";
        $reply->msg = "No row list available";
        echo json_encode($reply);
        return;
    }
    
    $list = $_SESSION['rowList'];
    
    if ($nextRowInfo->nextIndex <= 0 || $nextRowInfo->nextIndex >= count($list)) {
        $reply = new stdClass();
        $reply->response = "false";
        $reply->msg = "Row index is invalid";
        echo json_encode($reply);
        return;
    }
    
    $currentRow = $list[$nextRowInfo->nextIndex];
    $nextRowInfo->nextIndex += 1;
    if (count($list) > $nextRowInfo->nextIndex) {
        getResult($currentRow, $nextRowInfo->nextIndex);
    } else {
        getResult($currentRow, 0);
    }
}

function getResult($row, $nextIndex) {
    $row->response = "true";
    if ($nextIndex > 0) {
        $row->next = new stdClass();
        $row->next->uid = uniqid();
        $row->next->nextIndex = $nextIndex;
        $_SESSION['nextRow'] = $row->next;
    } else {
        unset($_SESSION['nextRow']);
    }
    echo json_encode($row);
}
