var formMode = "view";
var allowEdit = false;

$(document).ready(function () {
    "use strict";
    $("#searchItem").click(function () {
        searchAll($("#searchParam").val());
        return false;
    });
    $("#searchParam").keypress(function (event) {
        if (event.which === 13) {
            searchAll($("#searchParam").val());
        }
    });

    $("#edit").click(function () {
        startEdit();
        return false;
    });

    $("#newItem").click(function () {
        startNew();
        return false;
    });

    $("#cancel").click(function () {
        stopEdit();
        return false;
    });

    $("#save").click(function () {
        if (formMode === "edit") {
            saveEdit();
        } else {
            saveNew();
        }

        return false;
    });
});

function saveEdit() {
    var jjn = {};
    jjn._id = $("#_id").val();
    jjn.itemNo = $("#itemNo").val();
    jjn.itemName = $("#itemName").val();
    jjn.price = $("#price").val();
    jjn.notes = $("#notes").val();
    jjn.description = $("#description").val();
    jjn.foto = $("#foto").val();
    jjn.state = $("#state").prop("checked");
    send("jjn-save-edit.php", JSON.stringify(jjn), $("#message"), function (response) {
        console.log(response);
        var jresp;
        try {
            jresp = JSON.parse(response);
        } catch (err) {
            $("#message").html(response);
            return;
        }

        if (jresp.result !== "true") {
            $("#message").html(jresp.msg);
            return;
        }

        stopEdit();
        $("#message").html(jresp.msg);
    });
}

function saveNew() {
    var jjn = {};
    jjn._id = $("#_id").val();
    jjn.itemNo = $("#itemNo").val();
    jjn.itemName = $("#itemName").val();
    jjn.price = $("#price").val();
    jjn.notes = $("#notes").val();
    jjn.description = $("#description").val();
    jjn.foto = $("#foto").val();
    jjn.state = $("#state").prop("checked");
    send("jjn-save-new.php", JSON.stringify(jjn), $("#message"), function (response) {
        console.log(response);
        var jresp;
        try {
            jresp = JSON.parse(response);
        } catch (err) {
            $("#message").html(response);
            return;
        }

        if (jresp.result !== "true") {
            $("#message").html(jresp.msg);
            return;
        }

        stopEdit();
        $("#message").html(jresp.msg);
    });
}

function searchAll(param) {
    send("jjn-search.php", JSON.stringify(param), $("#msgSearch"), function (response) {
        console.log(response);
        allowEdit = false;
        disableEdit();
        var jjn;
        try {
            jjn = JSON.parse(response);
        } catch (err) {
            $("#msgSearch").html(response);
            return;
        }

        if (jjn.response === "true") {
            loadDataFrom(jjn);
            allowEdit = true;
            enableEdit();
            $("#msgSearch").html("");
        } else {
            $("#msgSearch").html(response);
        }
    });
}

function loadDataFrom(jjn) {
    $("#_id").val(jjn._id);
    $("#itemNo").val(jjn.itemNo);
    $("#itemName").val(jjn.itemName);
    $("#price").val(jjn.price);
    $("#notes").val(jjn.notes);
    $("#description").val(jjn.description);
    $("#foto").val(jjn.foto);
    $("#state").prop("checked", jjn.state);

    $("#_id").prop("placeholder", "");
    $("#itemNo").prop("placeholder", "");
    $("#itemName").prop("placeholder", "");
    $("#price").prop("placeholder", "");
    $("#notes").prop("placeholder", "");
    $("#description").prop("placeholder", "");
    $("#foto").prop("placeholder", "");
}

function enableEdit() {
    $("#edit").prop("disabled", "");
}

function disableEdit() {
    $("#edit").prop("disabled", "disabled");
}

function enableSearchAndNew() {
    $("#searchItem").prop("disabled", "");
    $("#searchParam").prop("readonly", "");
    $("#newItem").prop("disabled", "");
}

function disableSearchAndNew() {
    $("#searchItem").prop("disabled", "disabled");
    $("#searchParam").prop("readonly", "readonly");
    $("#newItem").prop("disabled", "disabled");
}

function startEdit() {
    formMode = "edit";
    startEditEnable();
    enableProp();
}

function startNew() {
    formMode = "new";
    startEditEnable();
    enableId();
    enableProp();
    allowEdit = false;
    initJjn();
}

function initJjn() {
    $("#_id").val("");
    $("#itemNo").val("");
    $("#itemName").val("");
    $("#price").val("");
    $("#notes").val("");
    $("#description").val("");
    $("#foto").val("");
    $("#state").prop("checked", true);

    $("#_id").prop("placeholder", "1");
    $("#itemNo").prop("placeholder", "RRAY");
    $("#itemName").prop("placeholder", "Risol Ayam");
    $("#price").prop("placeholder", "4000");
    $("#notes").prop("placeholder", "Isi ayam, wortel, dll");
    $("#description").prop("placeholder", "berisi adonan tepung, ayam, wortel, dll. Gurih dan Nikmat");
    $("#foto").prop("placeholder", "../kkweb/img/no-pict.png");
}

function enableId() {
    $("#_id").prop("readonly", "");
}

function enableProp() {
    $("#itemNo").prop("readonly", "");
    $("#itemName").prop("readonly", "");
    $("#price").prop("readonly", "");
    $("#notes").prop("readonly", "");
    $("#description").prop("readonly", "");
    $("#foto").prop("readonly", "");
    $("#state").prop("disabled", "");
}

function disableId() {
    $("#_id").prop("readonly", "readonly");
}

function disableProp() {
    $("#itemNo").prop("readonly", "readonly");
    $("#itemName").prop("readonly", "readonly");
    $("#price").prop("readonly", "readonly");
    $("#notes").prop("readonly", "readonly");
    $("#description").prop("readonly", "readonly");
    $("#foto").prop("readonly", "readonly");
    $("#state").prop("disabled", "disabled");
}

function startEditEnable() {
    disableEdit();
    disableSearchAndNew();
    $(".mode-edit").show();
}

function stopEdit() {
    $(".mode-edit").hide();
    enableSearchAndNew();
    disableId();
    disableProp();
    if (allowEdit) {
        enableEdit();
    }
}