<?php

require '../config/config-login.php';
require "../header.php";
require '../config/dbconf.php';
require '../config/globalcon.php';
require '../includes/functions.php';

try {
    $param = json_decode(file_get_contents('php://input'));
    $jjnDb = new JjnDb();
    $result = $jjnDb->insert($param);
    if ($result == "true") {
        $msg = "<div class=\"alert alert-info alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                . "Berhasil" .
                "</div>";
        $response = new stdClass();
        $response->result = $result;
        $response->msg = $msg;
        echo json_encode($response);
        return;
    }

    echo "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>\n";
    echo json_encode($result);
    echo "</div>";
} catch (Exception $ex) {
    echo "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
    . $ex . "</div>";
    if (isset($jjnDb)&& isset($jjnDb->conn)) {
        $jjnDb->conn->rollback();
    }
    return;
}