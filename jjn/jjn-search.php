<?php
require '../config/config-login.php';
require "../header.php";
require '../config/dbconf.php';
require '../config/globalcon.php';
require '../includes/functions.php';

try
{
    $param = json_decode(file_get_contents('php://input'));
    $jjnDb = new JjnDb();
    echo $jjnDb->search($param);
} catch (Exception $ex) {
    echo "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
    . $ex . "</div>";
    return;
}