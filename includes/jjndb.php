<?php

class JjnDb extends DbConn {

    public function __construct() {
        parent::__construct();
    }

    public function setInvalid($obj, $field, $msgText) {
        $invalidField = "invalid_" . $field;
        $obj->$invalidField = true;
        $obj->hasError = true;
        $msg = new stdClass();
        $msg->field = $field;
        $msg->msgText = $msgText;
        $obj->msgs[] = $msg;
    }

    public function required($obj, $field) {
        $invalidField = "invalid_" . $field;
        if (isset($obj->$invalidField) && $obj->$invalidField) {
            return false;
        }

        if ($obj->$field == NULL) {
            $this->setInvalid($obj, $field, "Required");
            return false;
        }

        if ($obj->$field == "") {
            $this->setInvalid($obj, $field, "Required");
            return false;
        }

        return true;
    }

    public function requiredInt($obj, $field) {
        if (!$this->required($obj, $field)) {
            return false;
        }

        $valInt = intval($obj->$field);
        if ($valInt <= 0) {
            $this->setInvalid($obj, $field, "Required");
            return false;
        }

        return true;
    }

    public function validate($jjnObj) {
        $jjnObj->hasError = false;
        $jjnObj->msgs = array();
        $jjnObj->invalids = array();
        $this->requiredInt($jjnObj, "_id");
        $this->required($jjnObj, "itemNo");
        $this->required($jjnObj, "itemName");
        $this->requiredInt($jjnObj, "price");

        return !($jjnObj->hasError);
    }

    public function insert($jjnObj) {
        if (!$this->validate($jjnObj)) {
            return $jjnObj->msgs;
        }
        $this->conn->beginTransaction();

        $stmtJajanan = $this->conn->prepare(
                "INSERT INTO jajanan (_id, kode, nama) \n"
                . "VALUES \n"
                . "(:_id, :kode, :nama)\n");
        $stmtJajanan->bindParam(':_id', $jjnObj->_id);
        $stmtJajanan->bindParam(':kode', $jjnObj->itemNo);
        $stmtJajanan->bindParam(':nama', $jjnObj->itemName);
        $stmtJajanan->execute();

        $stmtJajananProp = $this->conn->prepare(
                "INSERT INTO jajananprop (_id, propName, intVal, numVal, strVal, dateVal, bolVal) \n"
                . "VALUES \n"
                . "(:_id, :propName, :intVal, :numVal, :strVal, :dateVal, :bolVal)\n");

        $stmtJajananProp->bindParam(':_id', $jjnObj->_id);
        $this->insertIntProp($stmtJajananProp, "price", $jjnObj->price);
        $this->insertStrProp($stmtJajananProp, "notes", $jjnObj->notes);
        $this->insertStrProp($stmtJajananProp, "description", $jjnObj->description);
        $this->insertStrProp($stmtJajananProp, "foto", $jjnObj->foto);
        $this->insertBolProp($stmtJajananProp, "state", $jjnObj->state);

        $this->conn->commit();
        return "true";
    }

    public function insertIntProp($stmt, $propName, $propVal) {
        $propObj = new stdClass();
        $propObj->intVal = $propVal;
        $this->insertProp($stmt, $propName, $propObj);
    }

    public function insertNumProp($stmt, $propName, $propVal) {
        $propObj = new stdClass();
        $propObj->numVal = $propVal;
        $this->insertProp($stmt, $propName, $propObj);
    }

    public function insertStrProp($stmt, $propName, $propVal) {
        $propObj = new stdClass();
        $propObj->strVal = $propVal;
        $this->insertProp($stmt, $propName, $propObj);
    }

    public function insertDateProp($stmt, $propName, $propVal) {
        $propObj = new stdClass();
        $propObj->dateVal = $propVal;
        $this->insertProp($stmt, $propName, $propObj);
    }

    public function insertBolProp($stmt, $propName, $propVal) {
        $propObj = new stdClass();
        $propObj->bolVal = $propVal;
        $this->insertProp($stmt, $propName, $propObj);
    }

    public function insertProp($stmt, $propName, $propObj) {
        $stmt->bindParam(':propName', $propName);
        $null = NULL;
        if (isset($propObj->intVal)) {
            $stmt->bindParam(':intVal', $propObj->intVal);
        } else {
            $stmt->bindParam(':intVal', $null);
        }
        if (isset($propObj->numVal)) {
            $stmt->bindParam(':numVal', $propObj->numVal);
        } else {
            $stmt->bindParam(':numVal', $null);
        }
        if (isset($propObj->strVal)) {
            $stmt->bindParam(':strVal', $propObj->strVal);
        } else {
            $stmt->bindParam(':strVal', $null);
        }
        if (isset($propObj->dateVal)) {
            $stmt->bindParam(':dateVal', $propObj->dateVal);
        } else {
            $stmt->bindParam(':dateVal', $null);
        }
        if (isset($propObj->bolVal)) {
            $stmt->bindParam(':bolVal', $propObj->bolVal);
        } else {
            $stmt->bindParam(':bolVal', $null);
        }

        $stmt->execute();
    }

    public function update($jjnObj) {
        if (!$this->validate($jjnObj)) {
            return $jjnObj->msgs;
        }
        $this->conn->beginTransaction();

        $stmtJajanan = $this->conn->prepare(
                "UPDATE jajanan \n"
                . "SET \n"
                . "kode = :kode, \n"
                . "nama = :nama \n"
                . "WHERE _id = :_id \n");

        $stmtJajanan->bindParam(':_id', $jjnObj->_id);
        $stmtJajanan->bindParam(':kode', $jjnObj->itemNo);
        $stmtJajanan->bindParam(':nama', $jjnObj->itemName);
        $stmtJajanan->execute();

        $jjnOri = new stdClass();
        $jjnOri->_id = $jjnObj->_id;
        $this->loadProp($jjnOri);
        
        $this->saveJajananProp($jjnObj, $jjnOri, "price", "intVal");
        $this->saveJajananProp($jjnObj, $jjnOri, "notes", "strVal");
        $this->saveJajananProp($jjnObj, $jjnOri, "description", "strVal");
        $this->saveJajananProp($jjnObj, $jjnOri, "foto", "strVal");
        $this->saveJajananProp($jjnObj, $jjnOri, "state", "bolVal");

        $this->conn->commit();
        return "true";
    }

    public function saveJajananProp($jjnObj, $jjnOri, $propName, $propTypeVal) {
        $propNameSet = $propName . "_set";
        if (isset($jjnOri->$propNameSet)) {
            $this->updateJajananProp($jjnObj, $propName, $propTypeVal, $jjnObj->$propName);
        } else {
            $this->insertJajananProp($jjnObj, $propName, $propTypeVal, $jjnObj->$propName);
        }
    }

    public function updateJajananProp($jjnObj, $propName, $propTypeVal, $propVal) {
        $stmtJajananProp = $this->conn->prepare(
                "UPDATE jajananprop \n"
                . "SET intVal = :intVal, numVal = :numVal, strVal = :strVal, \n"
                . "dateVal = :dateVal, bolVal = :bolVal \n"
                . "WHERE _id = :_id \n"
                . "AND propName = :propName \n");

        $this->bindParamProp($stmtJajananProp, $jjnObj->_id, $propName, $propTypeVal, $propVal);
        $stmtJajananProp->execute();
    }

    public function insertJajananProp($jjnObj, $propName, $propTypeVal, $propVal) {
        $stmtJajananProp = $this->conn->prepare(
                "INSERT INTO jajananprop (_id, propName, intVal, numVal, strVal, dateVal, bolVal) \n"
                . "VALUES \n"
                . "(:_id, :propName, :intVal, :numVal, :strVal, :dateVal, :bolVal)\n");

        $this->bindParamProp($stmtJajananProp, $jjnObj->_id, $propName, $propTypeVal, $propVal);
        $stmtJajananProp->execute();
    }

    public function bindParamProp($stmtJajananProp, $id, $propName, $propTypeVal, $propVal) {
        $stmtJajananProp->bindParam(':_id', $id);
        $stmtJajananProp->bindParam(':propName', $propName);
        $null = NULL;
        $stmtJajananProp->bindParam(':intVal', $null);
        $stmtJajananProp->bindParam(':numVal', $null);
        $stmtJajananProp->bindParam(':strVal', $null);
        $stmtJajananProp->bindParam(':dateVal', $null);
        $stmtJajananProp->bindParam(':bolVal', $null);
        $stmtJajananProp->bindParam(':' . $propTypeVal, $propVal);
    }

    public function search($param) {
        $notFound = "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                . $param . " not found</div>";

        $stmtJajanan = $this->conn->prepare(
                "SELECT * from jajanan \n"
                . "WHERE _id = :_id \n"
                . "OR kode = :kode \n"
                . "OR nama = :nama \n"
                . "ORDER BY kode, nama \n");
        $stmtJajanan->bindParam(':_id', $param);
        $stmtJajanan->bindParam(':kode', $param);
        $stmtJajanan->bindParam(':nama', $param);
        $stmtJajanan->execute();
        $result = $stmtJajanan->fetchAll(PDO::FETCH_OBJ);
        if (!$result) {
            return $notFound;
        }

        foreach ($result as $row) {
            if ($row->_id > 0) {
                $row->response = "true";
                $row->itemNo = $row->kode;
                $row->itemName = $row->nama;
                $this->loadProp($row);
                return json_encode($row);
            }
        }

        return $notFound;
    }

    public function searchList($param) {
        $notFound = new stdClass();
        $notFound->response = "false";
        $notFound->msg = $param . "Not found";

        $stmtJajanan = $this->conn->prepare(
                "SELECT * from jajanan \n"
                . "ORDER BY kode, nama \n");
        $stmtJajanan->execute();
        $list = $stmtJajanan->fetchAll(PDO::FETCH_OBJ);
        if (!$list) {
            return $notFound;
        }

        foreach ($list as $key => $row) {
            if ($row->_id > 0) {
                $row->response = "true";
                $row->itemNo = $row->kode;
                $row->itemName = $row->nama;
                $this->loadProp($row);
            } else {
                unset($list[$key]);
            }
        }

        if (count($list) == 0) {
            return $notFound;
        }
        
        $result = new stdClass();
        $result->response = "true";
        $result->list = $list;

        return $result;
    }

    public function loadProp($row) {
        $stmtProp = $this->conn->prepare(
                "SELECT * from jajananprop \n"
                . "WHERE _id = :_id \n");
        $stmtProp->bindParam(':_id', $row->_id);
        $stmtProp->execute();
        $result = $stmtProp->fetchAll(PDO::FETCH_OBJ);
        if (!$result) {
            return;
        }
        foreach ($result as $prop) {
            $propName = $prop->propName;
            $propNameSet = $prop->propName . "_set";
            $row->$propName = $this->getVal($prop);
            $row->$propNameSet = true;
        }
    }

    public function getVal($prop) {
        if (isset($prop->intVal) && $prop->intVal != NULL) {
            return $prop->intVal;
        } else if (isset($prop->numVal) && $prop->numVal != NULL) {
            return $prop->numVal;
        } else if (isset($prop->strVal) && $prop->strVal != NULL) {
            return $prop->strVal;
        } else if (isset($prop->dateVal) && $prop->dateVal != NULL) {
            return $prop->dateVal;
        } else if (isset($prop->bolVal) && $prop->bolVal != NULL) {
            return $prop->bolVal;
        } else {
            return NULL;
        }
    }

}
