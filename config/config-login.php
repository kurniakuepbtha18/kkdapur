<?php

$url_root = "http://localhost/kkdapur/";
//Set this for global site use
$siteName = 'Kurnia Kue';
//Maximum Login Attempts
$max_attempts = 5;
//Timeout (in seconds) after max attempts are reached
$login_timeout = 300;

//Makes readable version of timeout (in minutes). Do not change.
$timeout_minutes = round(($login_timeout / 60), 1);
