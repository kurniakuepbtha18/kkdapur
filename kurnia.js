var url_root = "http://localhost/kkdapur/";
$(document).ready(function () {
    "use strict";
    
    init();
});

function init () {
    $("#adas").prop("href", url_root);
    $("#atrk").prop("href", url_root + "trk/");
    $("#ajjn").prop("href", url_root + "jjn/");
    
    page = "1";
    $.ajax({
        type: "POST",
        url: url_root + "login/valses.php",
        data: JSON.stringify(page),
        success: function (response) {
            console.log(response);
            $("#init").html(response);
        },
        error: function (errObj, errorThrown) {
            console.log(errObj);
            console.log(errorThrown);
            window.location = url_root + "login/";
        }
    });
}

function send(url, data, msgOut, successFun) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: successFun,
        error: function (errObj, errorThrown) {
            console.log(errObj);
            console.log(errorThrown);
            msgOut.html(
                    "<div class=\"alert alert-danger alert-dismissable\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                    + "<div>Text Status: " + errObj.status + "-" + errObj.statusText + "</div>"
                    + "<div>Error: " + errorThrown + "</div>"
                    + "</div>");
        },
        beforeSend: function () {
            msgOut.html("<p class='text-center'><img src='" + url_root + "images/ajax-loader.gif'></p>");
        }
    });
}

function requestJson(url, data, msgOut, receiverFun) {
    send(url, JSON.stringify(data), msgOut, function (response){
        console.log(response);
        var jsonObj;
        try {
            jsonObj = JSON.parse(response);
        } catch (err) {
            jsonObj = {};
            jsonObj.response = false;
            jsonObj.html = response;
            msgOut.html(response);
            receiverFun(jsonObj);
            return;
        }

        if (jsonObj.response === "true") {
            msgOut.html("");
        } else {
            msgOut.html(jsonObj.msg);
        }
        receiverFun(jsonObj);
    });
}