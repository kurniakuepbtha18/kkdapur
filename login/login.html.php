<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dapur Kurnia Kue</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <link href="../css/main.css" rel="stylesheet" media="screen">
    </head>

    <body>
        <div class="container">
            <form class="form-signin" name="form1" method="post">
                <h2 class="form-signin-heading">Silakan Masuk</h2>
                <input id="un" type="text" class="form-control" autofocus>
                <input id="ek" type="password" class="form-control" >
                <button name="Submit" id="submit" 
                        class="btn btn-lg btn-primary btn-block" 
                        type="submit">Masuk</button>

                <div id="message"></div>
            </form>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="../kurnia.js" ></script>
        <script src="login.js"></script>
    </body>
</html>