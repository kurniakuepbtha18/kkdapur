<?php

//DO NOT ECHO ANYTHING ON THIS PAGE OTHER THAN RESPONSE
//'true' triggers login success
ob_start();
require '../config/config-login.php';
require '../config/dbconf.php';
require '../config/globalcon.php';
require '../includes/functions.php';

// To protect MySQL injection
$username = filter_input(INPUT_POST, 'un', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'ek', FILTER_SANITIZE_STRING);
$response = '';
$loginCtl = new LoginForm;
$conf = new GlobalConf;
$lastAttempt = checkAttempts($username);
$max_attempts = $conf->max_attempts;
//First Attempt
if ($lastAttempt['lastlogin'] == '') {
    $lastlogin = 'never';
//    $loginCtl->insertAttempt($username);
    $response = $loginCtl->checkLogin($username, $password);
} elseif ($lastAttempt['attempts'] >= $max_attempts) {
    //Exceeded max attempts
    $loginCtl->updateAttempts($username);
    $response = $loginCtl->checkLogin($username, $password);
} else {
    $response = $loginCtl->checkLogin($username, $password);
}
//if ($lastAttempt['attempts'] < $max_attempts && $response != 'true') {
if ($response != 'true') {
//    $loginCtl->updateAttempts($username);
    $resp = new RespObj($username, $response);
    $jsonResp = json_encode($resp);
    echo $jsonResp;
} else {
    $resp = new RespObj($username, $response);
    $jsonResp = json_encode($resp);
    echo $jsonResp;
}
unset($resp, $jsonResp);
ob_end_flush();
