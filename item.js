$(document).ready(function () {
    "use strict";
    
    loadJjnList();
});

function loadJjnList () {
    var param = {};
    requestJson(url_root + "jjn/jjn-list.php", param, $("#ap"), jjnReceiver);
}

function jjnReceiver(jjnReply) {
    if (!jjnReply.response) {
        return;
    }
    
    krkjjn = $(".krkjjn").clone().appendTo($("#jjnlist")).removeClass("krkjjn");
    krkjjn.find(".fitemName").text(jjnReply.itemName);
    krkjjn.find(".fprice").text(jjnReply.price);
    krkjjn.find(".fnotes").text("");
    krkjjn.find(".fnotes").text(jjnReply.notes);
    krkjjn.find(".flink").prop("href", "#");
    krkjjn.find(".flink").prop("href", jjnReply.link);
    krkjjn.find(".fpic").prop("src", "../kkweb/img/no-pict.png");
    krkjjn.find(".fpic").prop("src", jjnReply.foto);
    
    if (jjnReply.next) {
        var param = {};
        param.next = jjnReply.next;
        requestJson(url_root + "jjn/jjn-list.php", param, $("#ap"), jjnReceiver);
    }
}